const express = require("express");
const session = require("express-session");
const handlebars = require("express-handlebars");
const cookieParser = require("cookie-parser");
const crypto = require("crypto");

//Create the express server
const app = express();
const authTokens = {};

//Assign a value to post variable
const port = 3005;

// to support URL-encoded bodies
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const users = [
  // This user is added to the array to avoid creating new user on each restart
  {
    firstName: "John",
    lastName: "Doe",
    email: "johndoe@email.com",
    // This is the SHA256 hash for value of `password`
    password: "XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=",
  },
];

app.use(
  session({
    secret: "Keep it secret",
    name: "uniqueSessionID",
    saveUninitialized: false,
    resave: true,
  })
);

app.use(cookieParser());

//Serve static files
app.use(express.static("public"));

app.use((req, res, next) => {
  const authToken = req.cookies["AuthToken"];
  req.user = authTokens[authToken];
  next();
});

//Set handlebar configurations
app.engine(
  "hbs",
  handlebars({
    extname: ".hbs",
    defaultLayout: "main.hbs",
  })
);

//Set app to use handlebar engine
app.set("view engine", "hbs");

app.get("/", (req, res) => {
  if (req.session.loggedIn) res.redirect("/protected");
  res.render("login");
});

//Redirect to login
app.get("/login", (req, res) => {
  res.render("login");
});

app.get("/", (req, res) => {
  res.render("login");
});

app.post(
  "/login",
  express.urlencoded({
    extended: true,
  }),
  (req, res, next) => {
    const { email, password } = req.body;
    const hashedPassword = getHashedPassword(password);

    const user = users.find((u) => {
      return u.email === email && hashedPassword === u.password;
    });

    // Actual implementation would check values in a database
    if (user) {
      res.locals.email = req.body.email;
      const authToken = generateAuthToken();
      authTokens[authToken] = email;
      res.cookie("AuthToken", authToken);
      next();
    } else {
      res.sendStatus(401);
      res.render("login", {
        message: "Invalid username or password",
        messageClass: "alert-danger",
      });
    }
  },
  (req, res) => {
    req.session.loggedIn = true;
    req.session.email = res.locals.email;
    console.log(req.session);
    res.redirect("/protected");
  }
);

app.get("/protected", (req, res) => {
  if (req.session.loggedIn) {
    res.render("protected", {
      email: req.session.email,
    });
  } else
    res.render("login", {
      message: "Please login to continue",
      messageClass: "alert-danger",
    });
});

const getHashedPassword = (password) => {
  const sha256 = crypto.createHash("sha256");
  const hash = sha256.update(password).digest("base64");
  return hash;
};

const generateAuthToken = () => {
  return crypto.randomBytes(30).toString("hex");
};

app.get("/logout", (req, res) => {
  req.session.destroy((err) => {});
  console.log("session.loggedIn after destroy: " + session.loggedIn);
  res.send("Thank you! Visit again");
});

// The 404 Route (ALWAYS Keep this as the last route)
app.get("*", function (req, res) {
  res.render("error");
});

//Make the app listen at port 3007
app.listen(port, () => {
  console.log(`App listening to port ${port}`);
});
